import os
import cv2
import numpy as np


mask = "/home/raul/Desktop/mahb/RESULTS/GENERATED_31/predictions/"
frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
contour_path = '/home/raul/Desktop/mahb/CONTOURS/COMPARE/Accuracy/'

def edge_detect(file_name, tresh_min, tresh_max):
    mask_path_name = file_name + ".png"

    if os.path.exists(mask + mask_path_name) == False:
        print("FILE DOES NOT EXIST: " + mask + file_name + ".png")
        return

    image = cv2.imread(mask + mask_path_name)
    #image = image * 255
    image = image.astype(np.uint8)

    
    im_bw = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    im_bw = cv2.resize(im_bw, (224,224))

    (thresh, im_bw) = cv2.threshold(im_bw, tresh_min, tresh_max, 0)
    contours, hierarchy = cv2.findContours(im_bw, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    frames = cv2.imread(frame + file_name + ".jpg")
    frames = cv2.resize(frames, (224,224))
    cv2.drawContours(frames, contours, -1, (0,255,255), 1)
    cv2.imwrite(contour_path + mask_path_name, frames)


manual_mask_path = '/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5_MANUAL_RDMASKS/'
prediction_mask_path = '/home/raul/Desktop/mahb/RESULTS/GENERATED_25/predictions/'

files = os.listdir(manual_mask_path)
file_ids = []
c = 0
for name in files:
    final_filename = os.path.splitext(name)[0]
    c += 1
    print(str(c) + ") " + final_filename)
    edge_detect(l, 128, 255)


