import cv2
import numpy as np
import matplotlib.pyplot as plt
import sys
from datetime import datetime
import os

#FP = red
#FN = blue
#TP - green

mask = "/home/raul/Desktop/mahb/RESULTS/unet/GENERATED_31/predictions/"
contour_path = '/home/raul/Desktop/mahb/CONTOURS/COMPARE/Accuracy/658/Conv2dTranspose/'
frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
#manual_mask_path = '/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5_MANUAL_RDMASKS/'
manual_mask_path = '/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5_MANUAL_RDMASKS/'


filename_date = datetime.now().strftime("%Y%m%d-%H%M%S")
filename = "/home/raul/mahb/project/logs/test_iou/" + filename_date + ".log"

TOTAL_TP = 0
TOTAL_FP = 0
TOTAL_FN = 0


with open(filename, 'w') as filehandle:
    for listitem in ['ID', 'TRUE_POSITIVE_[GREEN]', 'FALSE_POSITIVE_[RED]', 'FALSE_NEGATIVE_[BLUE]', 'IOU', 'PRECISION', 'RECALL']:
        filehandle.write('%s ' % listitem)
    filehandle.write('\n')


def iou_test(fname):
    global TOTAL_TP, TOTAL_FP, TOTAL_FN
    img = cv2.imread(frame + fname + ".jpg",-1)  # orthoframe
    gt = cv2.imread(manual_mask_path + fname + ".png",-1)  # ground truth mask
    prd = cv2.imread(mask + fname + ".png",-1)  # prediction mask

    print("image ID: %s" % fname)

    _,temp1=cv2.threshold(gt,127,255,0)
    _,temp2=cv2.threshold(prd,127,255,0)

    #plt.figure(2)
    #plt.title(fname)
    #plt.imshow(temp1)

    #plt.figure(3)
    #plt.title(fname)
    #plt.imshow(temp2)

     
    print(img.shape)
    temp1[temp1>0]=1  # groundtruth mask
    temp2[temp2>0]=1  # prediction mask

    # IoU computation
    maskTP,maskFP,maskFN=masks(temp1,temp2)
    tp,fp,fn=TPFPFN(maskTP,maskFP,maskFN)      
    iou,pr,rc=IoU(tp,fp,fn)    

    TOTAL_TP += tp
    TOTAL_FP += fp
    TOTAL_FN += fn 

    #plt.figure(4)
    #plt.title(fname)
    #plt.imshow(maskFP)

    #plt.figure(5)
    #plt.title(fname)
    #plt.imshow(maskFN)



    img[maskTP==1] = [0,255,0]
    img[maskFP==1] = [0,0,255] 
    img[maskFN==1] = [255,0,0]      

    print("total TP = %d, FP = %d, FN = %d" % (tp,fp,fn))
    print("overall IoU = %.2f, Pr = %.2f, Rc = %.2f" % (iou, pr, rc))

    with open(filename, 'a') as filehandle:
        for listitem in [fname, tp, fp, fn, iou, pr, rc]:
            filehandle.write('%s ' % listitem)
        filehandle.write('\n')

    
    cv2.imwrite(contour_path + fname + ".jpg", img)
    print(contour_path + fname + ".jpg")

    #plt.figure(1)
    #plt.title(fname)
    #plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))



    #plt.show()


def masks(msk_1,msk_2):
    # msk_1 is the mask that is supposed to be right (ground truth)
    # both masks should be binary, containing just ones and zeros
    TP=np.multiply(msk_1,msk_2)  # intersection
    FP = msk_2 - TP
    FN = msk_1 - TP

    return TP,FP,FN

def TPFPFN(TP,FP,FN):
    TP=np.sum(TP)
    FP=np.sum(FP)
    FN=np.sum(FN)
    return TP,FP,FN

def IoU(TP,FP,FN):
     if TP>0:  
        iou=TP/(TP+FP+FN)
        rc=TP/(TP+FN)
        pr=TP/(TP+FP)
     else:  # to avoid division by zero
            iou=0
            rc=0
            pr=0
     return iou,pr,rc


files = os.listdir(mask)
files_sorted = sorted(files)

for fname in files_sorted:
    name = os.path.splitext(fname)[0]
    if 'merged' not in name:
        iou_test(name)  

TOTAL_IOU,TOTAL_PR,TOTAL_RC=IoU(TOTAL_TP,TOTAL_FP,TOTAL_FN)

with open(filename, 'a') as filehandle:
    filehandle.write('\n')
    filehandle.write('\n')

    for listitem in ['TOTAL_IOU','TOTAL_PR','TOTAL_RC', 'TOTAL_TP','TOTAL_FP','TOTAL_FN']:
        filehandle.write('%s ' % listitem)
    filehandle.write('\n')
    for listitem in [TOTAL_IOU,TOTAL_PR,TOTAL_RC, TOTAL_TP,TOTAL_FP,TOTAL_FN]:
        filehandle.write('%s ' % listitem)
    filehandle.write('\n')



    #fname="20180914_062054_LD5-001"
    #main(fname)