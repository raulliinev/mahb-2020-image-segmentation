import os
import sys
import random
import logging

import numpy as np
import cv2
import matplotlib.pyplot as plt

import matplotlib

import tensorflow as tf
from tensorflow import keras

from PIL import Image

from tensorflow.keras import backend as K
from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import CSVLogger
from tensorflow.keras.models import model_from_json

from datetime import datetime

from Dataset import Dataset
from Datagen.predict import DataGen

from scripts.rene_rotate import (
    rotate, paddingSquare, verticalSplit, derotate, cropTop, getMinBoxCoords, getHighestPointWithNeighbors, getInitialAngleEstimate, rotateAndAdjust
)

seed = 2020
random.seed = seed
np.random.seed = seed
tf.seed = seed
image_size = 224
batch_size = 8

logging.basicConfig(filename='segmentation.log', filemode='a', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt = '%m/%d/%Y %I:%M:%S %p')

print("TensorFlow version: ", tf.__version__)

class Predict:
    def __init__(self):
        self.path = '/home/raul/mahb/alfa/model/'
        #self.model_name = '20200509-000015_epochs-100_imagesize-224_batchsize-8' # rotated and cut best result
        self.model_name= '20201225-101218_epochs-100_imagesize-224_batchsize-8' # Filtered images best result, without rotation
        self.output_folder = 'GENERATED_23'
        self.model = ''
        self.merged_result_path = "/home/raul/Desktop/mahb/RESULTS/" +self.output_folder+ "/predictions/merged/"
        self.result_path_plot = "/home/raul/Desktop/mahb/RESULTS/" +self.output_folder+ "/predictions/"
        self.rotated = False

    def loadModel(self):
        json_file = open(self.path + self.model_name+'.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)
        print("done")

        # load weights into new model
        self.model.load_weights(self.path + self.model_name+'.h5')
        print("Loaded model from disk")

        loss_var = self.iou_loss

        return self.model

    def iou_loss(self, y_true, y_pred):
        total_iou = 0
        total_iou = total_iou - K.sum(y_true * y_pred) / K.sum(1-(1-y_true) * (1-y_pred))
        return total_iou 

    def IOUcomp(self, msk_1,msk_2):
        # msk_1 is the mask that is supposed to be right
        # both masks should be binary, containing just ones and zeros
        TP=np.multiply(msk_1,msk_2)  # intersection
        FP = np.sum(msk_2 - TP)
        FN = np.sum(msk_1 - TP)
        TP = np.sum(TP)

        if TP>0:  
            iou=TP/(TP+FP+FN)
            rc=TP/(TP+FN)
            pr=TP/(TP+FP)
        else:  # to avoid division by zero
            iou=0
            rc=0
            pr=0
        return iou,pr,rc


p = Predict()
loaded_model = p.loadModel()
data = Dataset()
valid_ids = data.load_test_patch()
#valid_ids = data.load_nigula_patch()

for id_name in valid_ids:

    print(id_name)

    image_path = data.path + id_name + ".jpg"
    mask_path = data.path + id_name + ".mask.png"

    if os.path.exists(image_path) == False:
        logging.error(image_path + " does not exist")

    image_original_unchanged = cv2.imread(image_path, 1)
    image_mask = cv2.imread(mask_path, 1)
    image_original = cv2.imread(image_path, 1)


    image_original, angle = rotateAndAdjust(image_original)
    img_cropped = cropTop(image_original, 1600)

    img_L, img_R = verticalSplit(img_cropped)
    img_L = paddingSquare(img_L)
    img_R = paddingSquare(img_R)

    halves = []
    dim_halves = img_L.shape
    original_shape = image_original.shape

    #cv2.imwrite(p.merged_result_path + id_name + "_orig_L.png", img_L)
    #cv2.imwrite(p.merged_result_path + id_name + "_orig_R.png", img_R)
    
    k = 1
    for im in (img_L, img_R): 
        dim = (data.image_size, data.image_size)
        img_orig_scaled = cv2.resize(im, dsize=dim, interpolation=cv2.INTER_AREA)

        img_orig_scaled = np.divide(img_orig_scaled, 255.)

        nn_input = np.reshape(img_orig_scaled, (-1, data.image_size, data.image_size, 3))
        results = loaded_model.predict(nn_input)

        out_arr2 = np.reshape(results[0], (data.image_size, data.image_size))
        out_arr2 = cv2.resize(results[0], dsize=dim, interpolation=cv2.INTER_AREA)
        dim = (original_shape[0], original_shape[1])

        out_arr2 = out_arr2 * 255
        out_arr2 = out_arr2.astype(np.uint8)
        out_arr2 = cv2.resize(out_arr2, dsize=(dim_halves[0],dim_halves[1]), interpolation=cv2.INTER_AREA)
        reta, out_arr2 = cv2.threshold(out_arr2, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        halves.append(out_arr2)
        k = 2


    full = np.zeros((original_shape[0], original_shape[1]), dtype=np.uint8)
    img_left = halves[0][:,:original_shape[1]//2]
    img_right = halves[1][:,:original_shape[1]//2]
    img_left = cv2.flip(img_left, 1)
    full[1600:,:original_shape[0]//2] = img_left
    full[1600:,original_shape[0]//2:]= img_right
    full = derotate(full, angle)
    cv2.imwrite(p.merged_result_path + id_name + ".png", full)


    fig = plt.figure(figsize=(8,8))
    fig.subplots_adjust(hspace=0.4, wspace=0.4)

    ax = fig.add_subplot(1, 2, 1)
    ax.title.set_text('Original')
    ax.imshow(image_original_unchanged)


    ax = fig.add_subplot(1, 2, 2)
    ax.title.set_text('Prediction')
    ax.imshow(full)

    fig.savefig(p.result_path_plot + id_name + '_plot.png')
    plt.cla()
    plt.close(fig)