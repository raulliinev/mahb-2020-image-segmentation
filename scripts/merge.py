import os
import numpy as np

import cv2
from rene_rotate import (
    rotate, paddingSquare, verticalSplit, derotate, cropTop, getMinBoxCoords, getHighestPointWithNeighbors, getInitialAngleEstimate, rotateAndAdjust
)

img_path = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"

result_path = "/home/raul/Desktop/mahb/RESULTS/20180914_062054_LD5_GENERATED_13/predictions/"
merged_result_path = "/home/raul/Desktop/mahb/RESULTS/20180914_062054_LD5_GENERATED_13/predictions/merged"

files = os.listdir(img_path)
exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.'] 
file_ids = []
for name in files:
    if not any(exclude in name for exclude in exclude_list):
        file_ids.append(os.path.splitext(name)[0]) 

print(file_ids)

img_width, img_height = 4096, 4096

for img_id in file_ids:
    image_path = os.path.join(img_path, img_id) + ".jpg"
    mask_path = os.path.join(img_path, img_id) + ".mask" + '.png'

    image_original = cv2.imread(image_path, -1)
    image_mask = cv2.imread(mask_path, 0)
        

    image_mask, angle = rotateAndAdjust(image_mask)
    image_mask = cropTop(image_mask, 1600)

    img_L, img_R = verticalSplit(image_mask)
    img_L = paddingSquare(img_L)
    img_R = paddingSquare(img_R)

    halves = []
    dim_halves = img_L.shape
    original_shape = image_mask.shape

    #for im in (img_L, img_R):
    #    dim = (img_height, img_width)
    #    img_orig_scaled = cv2.resize(im, dsize=dim, interpolation=cv2.INTER_AREA)
    #    img_orig_scaled = cv2.cvtColor(img_orig_scaled, cv2.COLOR_BGR2RGB)
    #    img_orig_scaled = np.divide(img_orig_scaled, 255.)
    #    nn_input = np.reshape(img_orig_scaled, (-1, img_width, img_height, 3))
        #results = model.predict(nn_input, steps=1)
        #out_arr2 = np.reshape(results[0], (img_width, img_height))
        #out_arr2 = cv2.resize(results[0], dsize=dim, interpolation=cv2.INTER_AREA)
        # dim = (original_shape[0], original_shape[1])

        out_arr2 = out_arr2 * 255
        out_arr2 = out_arr2.astype(np.uint8)
        out_arr2 = cv2.resize(out_arr2, dsize=(dim_halves[0],dim_halves[1]), interpolation=cv2.INTER_AREA)
        reta, out_arr2 = cv2.threshold(out_arr2, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        halves.append(out_arr2)

    full = np.zeros((original_shape[0], original_shape[1]), dtype=np.uint8)
    #img_left = halves[0][:,:original_shape[1]//2]
    #img_right = halves[1][:,:original_shape[1]//2]
    img_left = cv2.imread(result_path + img_id + "_L.png", 0)
    img_right = cv2.imread(result_path + img_id + "_R.png", 0)
    img_left = cv2.flip(img_left, 1)
    full[1600:,:original_shape[0]//2] = img_left
    full[1600:,original_shape[0]//2:]= img_right
    full = derotate(full, angle)
    cv2.imwrite(merged_result_path + img_id + ".png", full)
