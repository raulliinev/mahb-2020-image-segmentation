import os
import cv2
from rene_rotate import (
    rotate, paddingSquare, verticalSplit, derotate, cropTop, getMinBoxCoords, getHighestPointWithNeighbors, getInitialAngleEstimate, rotateAndAdjust
)

ids_path = "/home/raul/mahb/alfa/files/TTY/filtered/"
img_path = "/home/raul/mahb/alfa/files/TTY/20180815/"
files = os.listdir(ids_path)

file_ids = []
for name in files:
    slittext = os.path.splitext(name)[0]
    file_ids.append(os.path.splitext(slittext)[0]) 

# file_ids

for img_id in file_ids:
    try:
        image_path = os.path.join(img_path, img_id[:img_id.index("-")], img_id) + ".jpg"
        mask_path = os.path.join(img_path, img_id[:img_id.index("-")], img_id) + ".mask" + '.png'
        example_result_path = os.path.join(img_path, img_id[:img_id.index("-")], img_id) + ".rdmask" + '.png'

        image_original = cv2.imread(image_path, -1)
        image_mask = cv2.imread(mask_path, 0)
        image_input = cv2.imread(example_result_path, 0)

        image_mask, angle = rotateAndAdjust(image_mask)
        image_input = rotate(image_input, angle)
        image_original = rotate(image_original, angle)
        image_mask = cropTop(image_mask, 1600)
        image_input = cropTop(image_input, 1600)
        image_original = cropTop(image_original, 1600)
        i_mask_L, i_mask_R = verticalSplit(image_mask)
        i_input_L, i_input_R = verticalSplit(image_input)
        i_original_L, i_original_R = verticalSplit(image_original)
        i_mask_L = paddingSquare(i_mask_L)
        i_mask_R = paddingSquare(i_mask_R)
        i_input_L = paddingSquare(i_input_L)
        i_input_R = paddingSquare(i_input_R)
        i_original_L = paddingSquare(i_original_L)
        i_original_R = paddingSquare(i_original_R)

        writepath_original = "/media/raul/Väline HDD/TTY/rotated/original/"
        writepath_rdmask = "//media/raul/Väline HDD/TTY/rotated/rdmask/"
        
        image_size = 224

        i_input_L = cv2.resize(i_input_L, (image_size, image_size))
        i_input_R = cv2.resize(i_input_R, (image_size, image_size))

        cv2.imwrite(writepath_rdmask + img_id + "_L.png", i_input_L)
        cv2.imwrite(writepath_rdmask + img_id + "_R.png", i_input_R)
        # cv2.imwrite(DIR_ORIGINAL_MASKS_OUTPUT + os.sep + myfile + LEFT_IMG, i_mask_L)
        # cv2.imwrite(DIR_ORIGINAL_MASKS_OUTPUT + os.sep + myfile + RIGHT_IMG, i_mask_R)
        # file_name_out = DIR_ORIGINALS_OUTPUT + os.sep + myfile

        i_original_L = cv2.resize(i_original_L, (image_size, image_size))
        i_original_R = cv2.resize(i_original_R, (image_size, image_size))

        cv2.imwrite(writepath_original + img_id + "_L.png", i_original_L)
        cv2.imwrite(writepath_original + img_id + "_R.png", i_original_R)

    except Exception as e:
        print("Exception with img: " + str(img_id) + " Exception: " + str(e))