import os
from shutil import copy2

path = "/home/raul/Desktop/last400/mahb/image/"
path3 = "/home/raul/Desktop/last400/mahb/rdmask/"

target = "/home/raul/mahb/alfa/files/TTY/20180815/"

files = os.listdir(path3)
for name in files:
    substr = os.path.splitext(name)[0]
    foldername = substr.split('-')[0]

    filename = os.path.splitext(os.path.splitext(name)[0])[0]

    targetpath = target + foldername + "/"

    if os.path.exists(targetpath):
        #copyfile()
        src = path + filename + ".jpg"
        copy2(src, targetpath)

        print("src: " + src + " dest: " + targetpath) 

        src = path3 + filename + ".rdmask.png"
        copy2(src, targetpath)

        print("src: " + src + " dest: " + targetpath)  
