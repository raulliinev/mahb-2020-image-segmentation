import os
import sys
import random
import logging

import numpy as np
import cv2
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow import keras

from tensorflow.keras import backend as K
from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import CSVLogger
from tensorflow.keras.models import model_from_json

from datetime import datetime

from Dataset import Dataset

seed = 2020
random.seed = seed
np.random.seed = seed
tf.seed = seed

logging.basicConfig(filename='segmentation.log', filemode='a', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt = '%m/%d/%Y %I:%M:%S %p')

print("TensorFlow version: ", tf.__version__)

class DataGen(keras.utils.Sequence):
    def __init__(self, ids, path, batch_size=8, image_size=128, custom_path = False):
        self.ids = ids
        self.path = path
        self.batch_size = batch_size
        self.image_size = image_size
        self.on_epoch_end()
        self.original_images = []
        self.masks = []
        self.rdmasks = []
        self.file_ids = []
        self.current_batch_images = ''
        self.custom_rdmask_path = custom_path

    def __load__(self, id_name):

        #if self.custom_rdmask_path == False:
        #    image_path = os.path.join(self.path, id_name[:id_name.index("-")], id_name) + ".jpg"
        #    mask_path = os.path.join(self.path, id_name[:id_name.index("-")], id_name) + ".rdmask" + '.png'
        #else:
        #    image_path = self.path + id_name + ".jpg"
        #    mask_path = self.custom_rdmask_path + id_name + ".png"

        image_path = self.path + id_name + ".jpg"
        mask_path = self.path + id_name + ".mask.png"

        #print(image_path)
        #print(mask_path)

        if os.path.exists(image_path) == False or os.path.exists(mask_path) == False:
            logging.error(image_path + " or " + mask_path + " does not exist")
            return None, None

        ## Reading Image
        image = cv2.imread(image_path, 1)

        try:
            image = cv2.resize(image, (self.image_size, self.image_size))
        except Exception as e:
            logging.info(image_path)
            logging.exception(e)
            return None, None
        
        mask = np.zeros((self.image_size, self.image_size, 1))
        
        _mask_path = mask_path
        _mask_image = cv2.imread(_mask_path, -1)
        try:
            _mask_image = cv2.resize(_mask_image, (self.image_size, self.image_size)) #128x128
            _mask_image = np.expand_dims(_mask_image, axis=-1)
            mask = np.maximum(mask, _mask_image)
        except Exception as e:
            logging.info(mask_path)
            logging.exception(e)
            return None, None
            
        ## Normalizing 
        image = image/255.0
        mask = mask/255.0
        
        return image, mask

    def __getitem__(self, index, getimagepath = False):
        if(index+1)*self.batch_size > len(self.ids):
            self.batch_size = len(self.ids) - index*self.batch_size
        
        files_batch = self.ids[index*self.batch_size : (index+1)*self.batch_size]

        #print("BATCH Print")
        #print(str(files_batch))

        if getimagepath is True:
            self.current_batch_images = files_batch
        
        image = []
        mask  = []

        count = 0
        
        for file_id in files_batch:
            _img, _mask = self.__load__(file_id)
            
            if _img is None:
                continue

            image.append(_img)
            mask.append(_mask)

            #count += 1
            #print("Processed: " + str(file_id))
            #print("Processed: " + str(count) + "; Total:  " + str(len(self.ids)) + "; File id: " + str(file_id))
            
        image = np.array(image)
        mask  = np.array(mask)
        
        return image, mask
    
    def on_epoch_end(self):
        pass
    
    def __len__(self):
        return int(np.ceil(len(self.ids)/float(self.batch_size)))

#ROAD SIDE DATA
image_size = 224
epochs = 75
batch_size = 8


data = Dataset()
valid_ids = data.load_third_patch()



# load json and create model
json_file = open('/home/raul/mahb/alfa/model/20200324-205743-UNetW-model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
print("done")

# load weights into new model
loaded_model.load_weights("/home/raul/mahb/alfa/model/20200324-205743-UNetW.h5")
print("Loaded model from disk")

valid_gen = DataGen(valid_ids, data.path, image_size=image_size, batch_size=batch_size)
X, Y = valid_gen.__getitem__(1, True)

# evaluate loaded model on test data
loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
score = loaded_model.evaluate(X, Y, verbose=0)
print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1]*100))
count = 1
for c in range(200):

    x, y = valid_gen.__getitem__(c, True)
    current_batch = valid_gen.current_batch_images

    for i in range(len(current_batch)):
        count = count + 1
        fig = plt.figure(figsize=(8,8))
        fig.subplots_adjust(hspace=0.4, wspace=0.4)

        print(str(count) + ") Batch size: " + str(len(current_batch)) + " Batch Image: " + current_batch[i])

        result = loaded_model.predict(x)

        #image_path = os.path.join(all_train_path, current_batch[i][:current_batch[i].index("-")], current_batch[i]) + ".jpg"

        #print(current_batch[i])

        result = result > 0.5

        #fig = plt.figure()
        #fig.subplots_adjust(hspace=0.4, wspace=0.4)

        #fig = plt.figure()
        #fig.subplots_adjust(hspace=0.4, wspace=0.4)

        #fig.suptitle(current_batch[i])

        #ax = fig.add_subplot(1, 2, 1)
        #ax.title.set_text('Mask')
        #ax.imshow(np.reshape(y[i]*255, (image_size, image_size)), cmap="gray")

        ax = fig.add_subplot(1, 2, 1)
        ax.title.set_text('Original')
        ax.imshow(x[i][:, :, [2, 1, 0]])


        ax = fig.add_subplot(1, 2, 2)
        ax.title.set_text('Prediction')
        ax.imshow(np.reshape(result[i]*255, (image_size, image_size)), cmap="gray")

        #fig.savefig('/home/raul/Desktop/mahb/ROAD_SIDE_DATA/RESULT/'+current_batch[i])
        fig.savefig('/home/raul/Desktop/mahb/20180914_062054_LD5_GENERATED/'+current_batch[i])
        plt.cla()
        plt.close(fig)