import cv2
import numpy as np
import os
import sys
from pathlib import Path

images = "/home/raul/Desktop/last400/mahb/image/"
target = "/home/raul/mahb/alfa/files/TTY/20180815/"


files = os.listdir(images)

c=0
lst = []
for name in files:
    final_filename = os.path.splitext(name)[0]
    fname = final_filename.split("-")[0]
    if fname in lst:
        continue
    c += 1
    foldername = target + "MANUAL2_" + fname
    if os.path.exists(foldername):
        print("EXISTS")
        continue
    print(str(c) + ") " + foldername)
    lst.append(fname)
    Path(foldername).mkdir(parents=True, exist_ok=True)