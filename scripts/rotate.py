import sys
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import math

rotcond = True
resrate = 0.1

def rotres(fname):

    path='D:/ramasks/' +  fname[:8] + '/' + fname[:19] +'/' # directory of source images

    # read the images
    img = cv2.imread(fname + '.jpg', -1)
    mask = cv2.imread(fname + '.mask.png', -1)
    rdmask = cv2.imread(fname + '.rdmask.png', -1)

    h,w,d=img.shape
    print("image file: %s" % path + fname + '.jpg')
    print("image size: %dx%d" % (w,h))

    if rotcond==True:
      img,mask2,rdmask,angle = rotate(img,mask,rdmask)
    if resrate<1: # making images smaller
      img = cv2.resize(img,None,fx=resrate, fy=resrate, interpolation = cv2.INTER_AREA) 
      hs, ws = img.shape[:2]
      mask2 = cv2.resize(mask2, (ws, hs), interpolation = cv2.INTER_AREA) 
      rdmask = cv2.resize(rdmask, (ws, hs), interpolation = cv2.INTER_AREA)
      print("final image size: %dx%d" % (ws,hs))

    cv2.imwrite("CNNin.jpg",  img)
    cv2.imwrite("CNNout.jpg",  rdmask)

 
    plt.figure(1)
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    plt.figure(2)
    plt.imshow(mask2)
    plt.figure(3)
    plt.imshow(rdmask)
    
    plt.show()


def rotate(img,mask,rdmask):
    # angle computation and stuff

    h,w,d=img.shape

    im2, contours, hierarchy = cv2.findContours(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    areas = [cv2.contourArea(c) for c in contours]
    max_index = np.argmax(areas)
    cnt=contours[max_index]

    rect = cv2.minAreaRect(cnt)

    angle = getangle(cnt)
#    angle=rect[2]
#    print("road angle is %s degrees" % angle)
    print("preliminary calculated angle: %.2f degrees" % angle)

    # necessary canvas enlargement
    angle2=math.atan(min(w,h)/max(w,h))
    bi=round((max(w,h)/math.cos(angle2)-max(w,h))/2)    
    angle2=angle2*180/math.pi

    # make it square first, bother about cutting later
    tb=bb=lb=rb=bi
    if w>h:
        tb+=round((w-h)/2)
        bb+=round((w-h)/2)
    else:
        lb+=round((h-w)/2)
        rb+=round((h-w)/2)

    padded=cv2.copyMakeBorder(img, top=tb, bottom=bb, left=lb, right=rb, borderType= cv2.BORDER_CONSTANT, value=[0,0,0] )
    padded_mask=cv2.copyMakeBorder(mask, top=tb, bottom=bb, left=lb, right=rb, borderType= cv2.BORDER_CONSTANT, value=[0,0,0] )
    padded_rdmask=cv2.copyMakeBorder(rdmask, top=tb, bottom=bb, left=lb, right=rb, borderType= cv2.BORDER_CONSTANT, value=[0,0,0] )

    image_center = tuple(np.array(padded.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    
    result_mask = cv2.warpAffine(padded_mask, rot_mat, padded_mask.shape[1::-1], flags=cv2.INTER_LINEAR)
    # find the contour from the mask and turn it into a ROI
    im2, contours, hierarchy = cv2.findContours(result_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    x,y,w,h = cv2.boundingRect(contours[0])
    
    mskCrop = result_mask[y:y+h,x:x+w]

    # check if it is properly rotated
    # compare the number of nonzero pixels in upper and lower halves
    z1=cv2.countNonZero(mskCrop[0:h//2,:])
    z2=cv2.countNonZero(mskCrop[h//2+1:h,:])

    if z1>z2:
        pass
        # print("nose down, correct")
    else:
        # correction
        # print("nose up, wrong")
        if abs(angle+180)<abs(angle-180):
          angle+=180
        else:
          angle-=180

        print("final angle: %.2f degrees" % angle)

        rot_mat = cv2.getRotationMatrix2D(image_center, angle+180, 1.0)
        result_mask = cv2.warpAffine(padded_mask, rot_mat, padded_mask.shape[1::-1], flags=cv2.INTER_LINEAR)
        # find the contour from the mask and turn it into a ROI
        im2, contours, hierarchy = cv2.findContours(result_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        x,y,w,h = cv2.boundingRect(contours[0])
        mskCrop = result_mask[y:y+h,x:x+w]


    result = cv2.warpAffine(padded, rot_mat, padded.shape[1::-1], flags=cv2.INTER_LINEAR)
    result_rdmask = cv2.warpAffine(padded_rdmask, rot_mat, padded_rdmask.shape[1::-1], flags=cv2.INTER_LINEAR)

    imCrop = result[y:y+h,x:x+w]
    rdmskCrop = result_rdmask[y:y+h,x:x+w]
    print("resulting image size: %dx%d" % (w,h))

    return imCrop,mskCrop,rdmskCrop,angle         

def getangle(contm):
    rectm = cv2.minAreaRect(contm)
    boxm = cv2.boxPoints(rectm) # getting the rectangle points
    hp = np.argmin(boxm[:,1])  # highest point
    hpm=hp - 1; hpp=hp + 1  # neighbors
    if hp==0: hpm = 3
    if hp==3: hpp = 0

#    print("\nmask properties")
#    print("the highest point index: %d" % hp)
#    print("distances: %.2f %.2f" % (distance(tuple(boxm[0]),tuple(boxm[1])), distance(tuple(boxm[0]),tuple(boxm[3]))))
    if distance(tuple(boxm[hp]),tuple(boxm[hpm])) > distance(tuple(boxm[hp]),tuple(boxm[hpp])):
#        print("points containing width are %d and %d" % (hp,hpm))
#        km=(boxm[hpm][1]-boxm[hp][1])/(boxm[hp][0]-boxm[hpm][0])
        if abs(boxm[hp][0]-boxm[hpm][0])<0.0001:  # to avoid division by zero
           km=10000
        else:
           km=(boxm[hpm][1]-boxm[hp][1])/(boxm[hp][0]-boxm[hpm][0])
    else:
#        print("points containing width are %d and %d" % (hp,hpp))
#        km=(boxm[hpp][1]-boxm[hp][1])/(boxm[hp][0]-boxm[hpp][0])
        if abs(boxm[hp][0]-boxm[hpp][0])<0.0001:  # to avoid division by zero
           km=10000
        else:
           km=(boxm[hpp][1]-boxm[hp][1])/(boxm[hp][0]-boxm[hpp][0])
    return -1*np.arctan(km)*180/np.pi

def distance(p1,p2):
    d=np.sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)
    return d

def derotate(rdmask,angle):

    return rdmask



if __name__ == "__main__":
    if len(sys.argv)>1:
        fname = sys.argv[1]
    else: 
        # default file name if invoked without an argument
        fname='20180815_100702_LD5-018'
    rotres(fname)
