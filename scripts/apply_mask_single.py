import cv2
import os
import numpy as np

mask = "/home/raul/Desktop/mahb/RESULTS/20180914_062054_LD5_GENERATED_9/predictions/20180914_062054_LD5-003resized-4096.png"
mask2 = "/home/raul/Desktop/mahb/RESULTS/20180914_062054_LD5_GENERATED_15/predictions/merged/20180914_062054_LD5-003.png" #rotated
frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/20180914_062054_LD5-003.jpg"
contour_path = "/home/raul/Desktop/mahb/CONTOURS/COMPARE/"

def edge_detect(mask, mask2, frame, tresh_min, tresh_max):
    loaded_mask_1 = cv2.imread(mask)
    loaded_mask_2 = cv2.imread(mask2)


    loaded_mask_1 = loaded_mask_1.astype(np.uint8)
    loaded_mask_2 = loaded_mask_2.astype(np.uint8)

    
    loaded_mask_1 = cv2.cvtColor(loaded_mask_1, cv2.COLOR_RGB2GRAY)
    loaded_mask_2 = cv2.cvtColor(loaded_mask_2, cv2.COLOR_RGB2GRAY)

    (thresh, loaded_mask_1) = cv2.threshold(loaded_mask_1, tresh_min, tresh_max, 0)
    (thresh, loaded_mask_2) = cv2.threshold(loaded_mask_2, tresh_min, tresh_max, 0)


    contours_1, hierarchy_1 = cv2.findContours(loaded_mask_1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours_2, hierarchy_2 = cv2.findContours(loaded_mask_2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)


    loaded_frame_image = cv2.imread(frame)
    #frames = cv2.resize(frames, (224,224))
    cv2.drawContours(loaded_frame_image, contours_1, -1, (0,255,255), 5)
    cv2.drawContours(loaded_frame_image, contours_2, -1, (0,0,255), 5)


    cv2.imwrite(contour_path + "result.png", loaded_frame_image)



edge_detect(mask, mask2, frame, 128, 255)