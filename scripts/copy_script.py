import os
import shutil

path = r"/home/raul/mahb/alfa/files/TTY/20180815/"
files = os.listdir(path)
target = r"/home/raul/mahb/alfa/files/TTY/filtered/"

exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.']
file_ids = []
for name in files:
    subfolder = "/home/raul/mahb/alfa/files/TTY/20180815/" + name
    subfolder_files = os.listdir(subfolder)
    for fname in subfolder_files:
        if not any(exclude in fname for exclude in exclude_list):
            file_id = os.path.splitext(fname)[0]
            file_path = os.path.join(path, file_id[:file_id.index("-")], file_id) + ".info" + '.jpg'
            #print(file_path)
            targetfile = target + os.path.join(file_id[:file_id.index("-")], file_id) + ".info" + '.jpg'
            shutil.copy2(file_path, target)
