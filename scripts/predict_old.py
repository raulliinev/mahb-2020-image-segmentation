import os
import sys
import random
import logging

import numpy as np
import cv2
import matplotlib.pyplot as plt

import matplotlib

import tensorflow as tf
from tensorflow import keras

from PIL import Image

from tensorflow.keras import backend as K
from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import CSVLogger
from tensorflow.keras.models import model_from_json

from datetime import datetime

from Dataset import Dataset

seed = 2020
random.seed = seed
np.random.seed = seed
tf.seed = seed

logging.basicConfig(filename='segmentation.log', filemode='a', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt = '%m/%d/%Y %I:%M:%S %p')

print("TensorFlow version: ", tf.__version__)

class DataGen(keras.utils.Sequence):
    def __init__(self, ids, path, batch_size=8, image_size=128, custom_path = False):
        self.ids = ids
        self.path = path
        self.batch_size = batch_size
        self.image_size = image_size
        self.on_epoch_end()
        self.original_images = []
        self.masks = []
        self.rdmasks = []
        self.file_ids = []
        self.current_batch_images = ''
        self.custom_rdmask_path = custom_path

    def __load__(self, id_name):

        image_path = self.path + id_name + ".jpg"

        if os.path.exists(image_path) == False:
            logging.error(image_path + " does not exist")
            return None, None

        ## Reading Image
        image = cv2.imread(image_path, 1)

        try:
            image = cv2.resize(image, (self.image_size, self.image_size))
        except Exception as e:
            logging.info(image_path)
            logging.exception(e)
            return None, None
            
        ## Normalizing 
        image = image/255.0
        
        return image

    def __getitem__(self, index, getimagepath = False):
        if(index+1)*self.batch_size > len(self.ids):
            self.batch_size = len(self.ids) - index*self.batch_size
        
        files_batch = self.ids[index*self.batch_size : (index+1)*self.batch_size]

        if getimagepath is True:
            self.current_batch_images = files_batch
        
        image = []
        mask  = []

        count = 0
        
        for file_id in files_batch:
            _img = self.__load__(file_id)
            
            if _img is None:
                continue

            image.append(_img)
            
        image = np.array(image)
        
        return image
    
    def on_epoch_end(self):
        pass
    
    def __len__(self):
        return int(np.ceil(len(self.ids)/float(self.batch_size)))

#ROAD SIDE DATA
image_size = 224
epochs = 75
batch_size = 8


data = Dataset()
valid_ids = data.load_test_patch()
#valid_ids = data.load_shtroads_patch()
#valid_ids = data.load_nigula_patch_2()

# load json and create model

#model_name = '20210101-173422_epochs-100_imagesize-224_batchsize-8'
#model_name = '20210313-164631_epochs-100_imagesize-224_batchsize-8'
#model_name = '20210314-113033_epochs-100_imagesize-224_batchsize-8'
#model_name = '20210314-212057_epochs-100_imagesize-224_batchsize-8'
#model_name = '20210315-091342_epochs-100_imagesize-224_batchsize-8'
model_name = '20210315-091342_epochs-100_imagesize-224_batchsize-8'
#output_folder = 'GENERATED_21'
#output_path = '/home/raul/Desktop/L22neNigula/Predict/before_train/norotate_nigula_14_2/'
output_path = '/home/raul/Desktop/mahb/RESULTS/GENERATED_31/'

json_file = open('/home/raul/mahb/alfa/model/'+model_name+'.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
print("done")

# load weights into new model
loaded_model.load_weights('/home/raul/mahb/alfa/model/'+model_name+'.h5')
print("Loaded model from disk")

valid_gen = DataGen(valid_ids, data.path, image_size=image_size, batch_size=batch_size)
#X = valid_gen.__getitem__(1, True)

# evaluate loaded model on test data

def iou_loss(y_true, y_pred):
    total_iou = 0
    total_iou = total_iou - K.sum(y_true * y_pred) / K.sum(1-(1-y_true) * (1-y_pred))
    return total_iou 


def IOUcomp(msk_1,msk_2):
    # msk_1 is the mask that is supposed to be right
    # both masks should be binary, containing just ones and zeros
    TP=np.multiply(msk_1,msk_2)  # intersection
    FP = np.sum(msk_2 - TP)
    FN = np.sum(msk_1 - TP)
    TP = np.sum(TP)

    if TP>0:  
        iou=TP/(TP+FP+FN)
        rc=TP/(TP+FN)
        pr=TP/(TP+FP)
    else:  # to avoid division by zero
        iou=0
        rc=0
        pr=0
    return iou,pr,rc

loss_var = iou_loss

#loaded_model.compile(optimizer=optimizers.Adadelta(learning_rate=1.0), loss="binary_crossentropy", metrics=["acc", loss_var])

#score = loaded_model.evaluate(X, Y, verbose=0)
#print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1]*100))
count = 1
for c in range(210):

    x = valid_gen.__getitem__(c, True)

    current_batch = valid_gen.current_batch_images

    for i in range(len(current_batch)):
        count = count + 1
        fig = plt.figure(figsize=(8,8))
        fig.subplots_adjust(hspace=0.4, wspace=0.4)

        print(str(count) + ") Batch size: " + str(len(current_batch)) + " Batch Image: " + current_batch[i])

        #print(str(x.shape))
        #print(str(type(x)))
        #sys.exit()

        result = loaded_model.predict(x)

        result = result > 0.5


        ax = fig.add_subplot(1, 2, 1)
        ax.title.set_text('Original')
        ax.imshow(x[i][:, :, [2, 1, 0]])


        ax = fig.add_subplot(1, 2, 2)
        ax.title.set_text('Prediction')
        ax.imshow(np.reshape(result[i]*255, (image_size, image_size)), cmap="gray")



        #fig.savefig('/home/raul/Desktop/mahb/ROAD_SIDE_DATA/RESULT/'+current_batch[i])
        #savefolder = '/home/raul/Desktop/mahb/RESULTS/'+output_folder+'/'
        savefolder = output_path
        fig.savefig(savefolder + current_batch[i])
        plt.cla()
        plt.close(fig)

        img_path = savefolder + 'predictions/'
        
        w=h=4096

        image_out = result[i]*255
        image_out = image_out.astype(np.uint8)
        large_image = cv2.resize(image_out, (w, h), interpolation = cv2.INTER_LINEAR)
        cv2.imwrite(img_path + current_batch[i] + ".png", large_image)