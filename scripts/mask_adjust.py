import cv2
import numpy as np
import os

#rd_mask_raw_path = "/home/raul/Desktop/L22neNigula/Manual/rdmask_raw/2/"
#mask_path = "/home/raul/Desktop/L22neNigula/Manual/mask/"
#target_path = "/home/raul/Desktop/L22neNigula/Manual/rdmask/2/"


rd_mask_raw_path = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5_MANUAL_RDMASKS_RAW/"
mask_path = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
target_path = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5_MANUAL_RDMASKS/"


files = os.listdir(rd_mask_raw_path)
file_ids = []
c = 0
for name in files:
    final_filename = os.path.splitext(name)[0]
    c += 1
    print(str(c) + ") " + final_filename)

    mask = cv2.imread(mask_path + final_filename + ".mask.png" , 0)
    rd_raw_mask = cv2.imread(rd_mask_raw_path + final_filename + ".png", 0)

    #print(mask.shape)
    #print(mask.dtype)

    #print(rd_raw_mask.shape)
    #print(rd_raw_mask.dtype)

    rd_mask = cv2.bitwise_and(rd_raw_mask, rd_raw_mask, mask=mask)
    cv2.imwrite(target_path + final_filename + ".png", rd_mask)
