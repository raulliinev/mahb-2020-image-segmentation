import os
import cv2

from Dataset import Dataset


image_size = 224
path = "/home/raul/mahb/alfa/files/TTY/20180815/"

target_image = '/home/raul/mahb/alfa/files/TTY/224x224_RGB/manual/original/'
target_mask = '/home/raul/mahb/alfa/files/TTY/224x224_RGB/manual/rdmask/'

data = Dataset()
train_ids, valid_ids = data.load_filtered_data()

#print(str(len(train_ids) + len(valid_ids)))

count = 0
# train_ids and valid_ids both
for id_name in train_ids:
    if 'MANUAL2' in id_name:
        continue

    count = count + 1
    image_path = os.path.join(path, id_name[:id_name.index("-")], id_name) + ".jpg"
    mask_path = os.path.join(path, id_name[:id_name.index("-")], id_name) + ".rdmask" + '.png'

    if os.path.exists(mask_path) == False or os.path.exists(image_path) == False:
        print("DOESNT EXIST: " + image_path)
        print("DOESNT EXIST: " + mask_path)
        continue

    try:
        image = cv2.imread(image_path, 1)
        original = cv2.resize(image, (image_size, image_size))
        cv2.imwrite(target_image + id_name + ".png", original)

        rdmask_image = cv2.imread(mask_path, -1)
        rdmask = cv2.resize(rdmask_image, (image_size, image_size))
        cv2.imwrite(target_mask + id_name + ".png", rdmask)
    except Exception as e:
        print("EXCEPTION: " + image_path)
        print("EXCEPTION: " + mask_path)
        print(str(e))


    print(str(count) + ") DONE: " + id_name)
