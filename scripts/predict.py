""" Get road predictions from orthoframes.
* Possible to predict with one frame or multiple.
"""

import os
import sys
import random

import numpy as np
import cv2
import tensorflow as tf
from tensorflow.keras.models import model_from_json

SEED = 2020
random.seed = SEED
np.random.seed = SEED
tf.seed = SEED

IMAGE_SIZE = 224

def get_test_ids(path):
    """Get Test dataset ids

    Args:
        path (str): Test dataset frames path

    Returns:
        list: File ids witout extensions
    """
    files = os.listdir(path)

    exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.']
    file_ids = []
    for name in files:
        if not any(exclude in name for exclude in exclude_list):
            file_ids.append(os.path.splitext(name)[0]) 

    return file_ids

IMAGE_PATH = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
TEST_IDS = get_test_ids(IMAGE_PATH)
MODEL_PATH = '/home/raul/mahb/project/model/'

#MODEL_NAME = '20210315-091342_epochs-100_imagesize-224_batchsize-8'
MODEL_NAME =  '20210506-110823_epochs-100_imagesize-224_batchsize-8'
OUTPUT_PATH = '/home/raul/Desktop/mahb/RESULTS/unet/GENERATED_31/'

JSON_FILE = open(MODEL_PATH + MODEL_NAME+'.json', 'r')
LOADED_MODEL_JSON = JSON_FILE.read()
JSON_FILE.close()
LOADED_MODEL = model_from_json(LOADED_MODEL_JSON)

LOADED_MODEL.load_weights(MODEL_PATH + MODEL_NAME+'.h5')
print("Model loaded from disk")


COUNT = 1

for image_id in TEST_IDS:
    count = COUNT + 1
    #image_id = "20180914_062054_LD5-030"
    print(IMAGE_PATH + image_id + ".jpg")

    image_input = cv2.imread(IMAGE_PATH + image_id + ".jpg", 1)
    image_resize = cv2.resize(image_input, (IMAGE_SIZE, IMAGE_SIZE))

    print(str(count) + ") " + str(image_id) + " Shape: " + str(image_resize.shape))
    image_div = image_resize / 255.0

    image_np = np.array([image_div])

    pr = LOADED_MODEL.predict(image_np)[0]
    result = pr > 0.5

    img_path = OUTPUT_PATH + 'predictions/'
    w = h = 4096

    image_out = result*255
    image_out = image_out.astype(np.uint8)
    large_image = cv2.resize(image_out, (w, h), interpolation=cv2.INTER_LINEAR)
    cv2.imwrite(img_path + image_id + ".png", large_image)
    #sys.exit()
