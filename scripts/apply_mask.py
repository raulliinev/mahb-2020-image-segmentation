import cv2
import os
import numpy as np


def load_images_from_folder_to_list(folder):
    images = []
    imglist = []
    for filename in os.listdir(folder):
        name = os.path.splitext(filename)[0]
        #if 'mask' not in name:
        imglist.append(name)
    return imglist


#mask = "/home/raul/Desktop/mahb/RESULTS/20180914_062054_LD5_GENERATED_18/predictions/"
#frame = "/home/raul/Desktop/mahb/INPUT/shtroads/"
#mask = "/home/raul/Desktop/mahb/RESULTS/20180914_062054_LD5_GENERATED_9/predictions/"
#frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"

#mask = "/home/raul/Desktop/mahb/RESULTS/20180914_062054_LD5_GENERATED_15/predictions/merged/"
#frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
#contour_path = "/home/raul/Desktop/mahb/CONTOURS/Rotated_18_complexity_v1/"

#mask = "/home/raul/Desktop/mahb/RESULTS/GENERATED_22/predictions/merged/"
#frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
#contour_path = "/home/raul/Desktop/mahb/CONTOURS/Rotated_22_adam_0.0001/"

#mask = "/home/raul/Desktop/mahb/RESULTS/GENERATED_23/predictions/merged/"
#frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
#contour_path = "/home/raul/Desktop/mahb/CONTOURS/Rotated_23_scheduling_lr_2e-1/"

#mask = "/home/raul/Desktop/mahb/RESULTS/GENERATED_24/predictions/"
#frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
#contour_path = '/home/raul/Desktop/mahb/CONTOURS/NoRotate_24_manual_masks/'

#mask = "/home/raul/Desktop/mahb/RESULTS/GENERATED_25/predictions/"
#frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
#contour_path = '/home/raul/Desktop/mahb/CONTOURS/NoRotate_25_manual_mask_adadelta/'

#mask = "/home/raul/Desktop/mahb/RESULTS/GENERATED_25/predictions/"
#frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
#contour_path = '/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5_MANUAL_RDMASKS'

mask = "/home/raul/Desktop/mahb/RESULTS/GENERATED_29/predictions/"
frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
contour_path = '/home/raul/Desktop/mahb/CONTOURS/658/'

#mask = "/home/raul/Desktop/mahb/RESULTS/GENERATED_19/predictions/merged/"
#frame = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
#contour_path = "/home/raul/Desktop/mahb/CONTOURS/Rotated_19_adam_comp1/"

#mask = "/home/raul/Desktop/L22neNigula/Manual/rdmask/2/"
#frame = "/home/raul/Desktop/L22neNigula/Manual/original/"
#contour_path = "/home/raul/Desktop/L22neNigula/Manual/Predictions/2/"

def edge_detect(file_name, tresh_min, tresh_max):
    mask_path_name = file_name + ".png"

    if os.path.exists(mask + mask_path_name) == False:
        print("FILE DOES NOT EXIST: " + mask + file_name + ".png")
        return

    image = cv2.imread(mask + mask_path_name)
    #image = image * 255
    image = image.astype(np.uint8)

    
    im_bw = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    im_bw = cv2.resize(im_bw, (224,224))

    (thresh, im_bw) = cv2.threshold(im_bw, tresh_min, tresh_max, 0)
    contours, hierarchy = cv2.findContours(im_bw, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    frames = cv2.imread(frame + file_name + ".jpg")
    frames = cv2.resize(frames, (224,224))
    cv2.drawContours(frames, contours, -1, (0,255,255), 1)
    cv2.imwrite(contour_path + mask_path_name, frames)


lst = load_images_from_folder_to_list(mask)
print(lst)

c = 0
for l in lst:
    c += 1
    print(str(c) + ") " + l)
    edge_detect(l, 128, 255)

