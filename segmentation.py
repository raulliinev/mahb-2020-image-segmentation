import os
import sys
import random

import matplotlib.pyplot as plt

from tensorflow.keras import backend as K
from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import CSVLogger, EarlyStopping

from datetime import datetime

from keras_segmentation.models.unet import vgg_unet

from Config import tf, np, get_logger
from Datagen.train2 import DataGen_2
from Dataset import Dataset
from Callbacks import OnEpochEnd

from contextlib import redirect_stdout

print("TensorFlow version: ", tf.__version__)

logger = get_logger()

tf.config.list_physical_devices('GPU')
#if tf.test.gpu_device_name(): 
#    print('Default GPU Device:{}'.format(tf.test.gpu_device_name()))
#else:
#   print("Please install GPU version of TF")
#tf.test.gpu_device_name()

#from tensorflow.python.client import device_lib
#def get_available_devices():
#    local_device_protos = device_lib.list_local_devices()
#    return [x.name for x in local_device_protos]
#print(get_available_devices()) 


data = Dataset()
train_ids, valid_ids = data.load_main_data()


data = Dataset()
train_ids, valid_ids = data.load_filtered_rotated_data()

data = Dataset()
train_ids, valid_ids = data.load_filtered_data()

#ROAD SIDE DATA
data = Dataset()
train_ids, valid_ids = data.load_complicated_data()

#TURNS AND SHADED MAUNAL MASKS
data = Dataset()
train_ids, valid_ids = data.load_filtered_data_224_657_rgb()


# 133/525
# new 97/360

count = 0
for idx in valid_ids:
    if 'MANUAL2' in idx:
        count = count + 1
        #print(count)
        #print(str(count) + ") " + idx)
print(count)


#gen = DataGen(train_ids, train_path, batch_size=batch_size, image_size=image_size)
#x, y = gen.__getitem__(0)
gen_all = DataGen_2(train_ids, data.path, batch_size=data.batch_size, image_size=data.image_size)
x, y = gen_all.__getitem__(0)
print(x.shape, y.shape)

r = random.randint(0, len(x)-1)

fig = plt.figure()
fig.subplots_adjust(hspace=0.4, wspace=0.4)
ax = fig.add_subplot(1, 2, 1)
ax.imshow(x[r])
ax = fig.add_subplot(1, 2, 2)
ax.imshow(np.reshape(y[r], (data.image_size, data.image_size)), cmap="gray")

def down_block(x, filters, kernel_size=(3, 3), padding="same", strides=1):
    c = tf.keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(x)
    c = tf.keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(c)
    p = tf.keras.layers.MaxPool2D((2, 2), (2, 2))(c)
    return c, p

def up_block(x, skip, filters, kernel_size=(3, 3), padding="same", strides=1):
    #us = tf.keras.layers.UpSampling2D((2, 2))(x)
    #cv = tf.keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(us)
    deconv2 = tf.keras.layers.Conv2DTranspose(filters, kernel_size, strides=(2, 2), padding=padding)(x)
    concat = tf.keras.layers.Concatenate()([deconv2, skip])
    c = tf.keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(concat)
    c = tf.keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(c)
    return c

def bottleneck(x, filters, kernel_size=(3, 3), padding="same", strides=1):
    c = tf.keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(x)
    c = tf.keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(c)
    return c

def UNet():
    f = [16, 32, 64, 128, 256]
    inputs = tf.keras.layers.Input((data.image_size, data.image_size, 3))
    
    p0 = inputs
    c1, p1 = down_block(p0, f[0]) #128 -> 64
    c2, p2 = down_block(p1, f[1]) #64 -> 32
    c3, p3 = down_block(p2, f[2]) #32 -> 16
    c4, p4 = down_block(p3, f[3]) #16->8
    
    bn = bottleneck(p4, f[4])
    
    u1 = up_block(bn, c4, f[3]) #8 -> 16
    u2 = up_block(u1, c3, f[2]) #16 -> 32
    u3 = up_block(u2, c2, f[1]) #32 -> 64
    u4 = up_block(u3, c1, f[0]) #64 -> 128
    
    outputs = tf.keras.layers.Conv2D(1, (1, 1), padding="same", activation="sigmoid")(u4)
    model = tf.keras.models.Model(inputs, outputs)
    return model



model = UNet()

#Also takes into account true negative numbers
def iou_loss(y_true, y_pred):
    total_iou = 0
    total_iou = total_iou - K.sum(y_true * y_pred) / K.sum(1-(1-y_true) * (1-y_pred))
    return total_iou 

function_iou_loss = iou_loss


#Same as upper method. This method excludes division with zero was used as a loss function. Did not improve
def rene_iou(y_true, y_pred):
    t = K.reshape(y_true, (-1, 1))
    p = K.reshape(y_pred, (-1, 1))
    tn = 1.0 - t
    pn = 1.0 - p
    return -1.0 * tf.math.divide_no_nan(K.sum(t*p), K.sum(1.0 - tn*pn))

lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate=3e-4,
    decay_steps=10000,
    decay_rate=0.9)

model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001), 
    loss="binary_crossentropy", 
    metrics=["acc", function_iou_loss]
) 

#pole päris õige binary_crossentropy saab lisada metricu ja kasutada kulufunktsiooni. Näitab mulle kas liigun. Precision recall. 
# optimizer võiks olla adam asemel adadelta. Learning rate peaks olema 1. acc asmel eraldi klass
model.summary()

#with open('modelsummary.txt', 'w') as f:
#    with redirect_stdout(f):
#        model.summary()


train_gen = DataGen_2(train_ids, data.path, image_size=data.image_size, batch_size=data.batch_size)
valid_gen = DataGen_2(valid_ids, data.path, image_size=data.image_size, batch_size=data.batch_size)
epochs = 100


train_steps = len(train_ids)//data.batch_size
valid_steps = len(valid_ids)//data.batch_size

filename_date = datetime.now().strftime("%Y%m%d-%H%M%S")

csv_logger = CSVLogger('/home/raul/mahb/project/logs/training/'+filename_date+'.log')

print("Training begin time: " + str(datetime.now().strftime("%Y.%m.%d %H:%M:%S")))

#learning_rate_callback = tf.keras.callbacks.LearningRateScheduler(lr_schedule)

log_dir = "logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

#tf.keras.callbacks.TensorBoard(
#    log_dir='logs', update_freq='epoch',
#   profile_batch=0,                      # <-- default value is 2
#)

model.fit(
    train_gen, 
    validation_data=valid_gen, 
    steps_per_epoch=None, 
    validation_steps=valid_steps, 
    epochs=epochs, 
    callbacks=[
        csv_logger, 
        OnEpochEnd([train_gen.on_epoch_end]),
        tensorboard_callback
    ], 
    shuffle=True
)

print("Training end time: " + str(datetime.now().strftime("%Y.%m.%d %H:%M:%S")))

## Save the Weights
filename = filename_date + "_epochs-" + str(epochs) + "_imagesize-" + str(data.image_size) + "_batchsize-" + str(data.batch_size)

model.save_weights("model/" + filename + ".h5")

file_model = "model/" + filename + ".json"
model_json = model.to_json()
with open(file_model, "w") as json_file:
    json_file.write(model_json)

filepath = "model/"+filename+".tf"
tf.keras.models.save_model(
    model, filepath, overwrite=True, include_optimizer=True, save_format=None,
    signatures=None, options=None
)

desc_model = "generator/" + filename + ".txt"
with open(desc_model, "w") as desc_file:
    desc_file.write(data.path)

## Dataset for prediction
x, y = valid_gen.__getitem__(1)
result = model.predict(x)

#treshold tulemused ja tulemustel ta teeb tresholdi ära osa pilt kümme pilti. andmete struktuur jääb samaks. 0.32 paneb väärtuseks kui see jääb alla. pixli tasemel käib otsus. 
result = result > 0.5

valid_gen = DataGen(valid_ids, all_train_path, image_size=image_size, batch_size=batch_size, custom_path = all_train_path_rdmasks)

x, y = valid_gen.__getitem__(5, True)

current_batch = valid_gen.current_batch_images

fig = plt.figure(figsize=(8,8))
fig.subplots_adjust(hspace=0.4, wspace=0.4)

for i in range(1):

    ax = fig.add_subplot(2, 2, 1)
    ax.title.set_text('Original')
    ax.imshow(x[i][:, :, [2, 1, 0]])

    result = model.predict(x)

    #image_path = os.path.join(all_train_path, current_batch[i][:current_batch[i].index("-")], current_batch[i]) + ".jpg"

    print(current_batch[i])

    result = result > 0.5

    #fig = plt.figure()
    #fig.subplots_adjust(hspace=0.4, wspace=0.4)

    fig = plt.figure(figsize=(8,8))
    fig.subplots_adjust(hspace=0.4, wspace=0.4)

    ax = fig.add_subplot(1, 2, 1)
    ax.title.set_text('Mask')
    ax.imshow(np.reshape(y[i]*255, (image_size, image_size)), cmap="gray")

    ax = fig.add_subplot(1, 2, 2)
    ax.title.set_text('Prediction')
    ax.imshow(np.reshape(result[i]*255, (image_size, image_size)), cmap="gray")


fig = plt.figure()
fig.subplots_adjust(hspace=0.4, wspace=0.4)

ax = fig.add_subplot(1, 2, 1)
ax.imshow(np.reshape(y[4]*255, (data.image_size, data.image_size)), cmap="gray")

ax = fig.add_subplot(1, 2, 2)
ax.imshow(np.reshape(result[4]*255, (data.image_size, data.image_size)), cmap="gray")


