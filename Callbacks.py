import tensorflow as tf
from tensorflow.keras import backend as K
from datetime import datetime
import os

class OnEpochEnd(tf.keras.callbacks.Callback):
  def __init__(self, callbacks):
    self.callbacks = callbacks
    self.filename_date = datetime.now().strftime("%Y%m%d-%H%M%S")

  def on_epoch_end(self, epoch, logs=None):

    filename = "decay/" + self.filename_date + ".log"
    if os.path.exists(filename):
      append_write = 'a' # append if already exists
    else:
        append_write = 'w' # make a new file if not

    
    with open(filename, append_write) as file_open:
      file_open.write("{:0.7f}".format(self.model.optimizer._decayed_lr(tf.float32).numpy()))

    for callback in self.callbacks:
      callback()