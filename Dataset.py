import os

class Dataset:
    def __init__(self):
        self.epochs = 10
        self.batch_size = 8
        self.image_size = 224
        self.path = ''

    def load_main_data(self):
        self.path = "/home/raul/mahb/alfa/files/TTY/20180815/"
        files = os.listdir(self.path)
        
        exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.'] 
        file_ids = []
        for name in files:
            subfolder = "/home/raul/mahb/alfa/files/TTY/20180815/" + name
            subfolder_files = os.listdir(subfolder)
            for fname in subfolder_files:
                if not any(exclude in fname for exclude in exclude_list):
                    file_ids.append(os.path.splitext(fname)[0]) 
            
        val_data_size_percentage = 0.2 #20%

        val_data_size = int(len(file_ids) * val_data_size_percentage)

        valid_ids = file_ids[:val_data_size]
        train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d, files for training %d -----------" %(len(valid_ids), len(train_ids)))

        return train_ids, valid_ids

    def load_filtered_data(self):
        path = "/home/raul/mahb/alfa/files/TTY/filtered/"
        files = os.listdir(path)
        
        file_ids = []
        for name in files:
            slittext = os.path.splitext(name)[0]
            file_ids.append(os.path.splitext(slittext)[0]) 
        
        
        val_data_size_percentage = 0.2 #20%
        val_data_size = int(len(file_ids) * val_data_size_percentage)

        valid_ids = file_ids[:val_data_size]
        train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d, files for training %d -----------" %(len(valid_ids), len(train_ids)))

        self.path = "/home/raul/mahb/alfa/files/TTY/20180815/"

        return train_ids, valid_ids

    def load_filtered_data_224_657_rgb(self):

        self.path = "/home/raul/mahb/project/files/TTY/224x224_RGB/manual/"
        files = os.listdir(self.path + 'original')
        
        file_ids = []
        for name in files:
            slittext = os.path.splitext(name)[0]
            file_ids.append(os.path.splitext(slittext)[0]) 
        
        
        val_data_size_percentage = 0.2 #20%
        val_data_size = int(len(file_ids) * val_data_size_percentage)

        valid_ids = file_ids[:val_data_size]
        train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d, files for training %d -----------" %(len(valid_ids), len(train_ids)))

        return train_ids, valid_ids


    def load_complicated_data(self):
        #ROAD SIDE DATA
        self.path = "/home/raul/Desktop/mahb/ROAD_SIDE_DATA/ORIGINALS/"
        #all_train_path_rdmasks = "/home/raul/Desktop/mahb/ROAD_SIDE_DATA/RDMASKS/"
        #all_train_files = !ls {all_train_path}
        files = os.listdir(self.path)
        
        file_ids = []
        for name in files:
            file_ids.append(os.path.splitext(name)[0])

        val_data_size = 300

        valid_ids = file_ids[:val_data_size]
        train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d, files for training %d -----------" %(len(valid_ids), len(train_ids)))

        return train_ids, valid_ids

    def load_third_patch_rotated(self):
        #ROAD SIDE DATA
        self.path = "/home/raul/Desktop/mahb/INPUT/ROTATED/original/"
        files = os.listdir(self.path)

        exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.'] 
        file_ids = []
        for name in files:
            if not any(exclude in name for exclude in exclude_list):
                file_ids.append(os.path.splitext(name)[0]) 

        val_data_size = 300

        #valid_ids = file_ids[:val_data_size]
        #train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d -----------" %(len(file_ids)))

        return file_ids

    def load_test_patch(self):
        #ROAD SIDE DATA
        self.path = "/home/raul/Desktop/mahb/INPUT/20180914_062054_LD5/"
        files = os.listdir(self.path)

        exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.'] 
        file_ids = []
        for name in files:
            if not any(exclude in name for exclude in exclude_list):
                file_ids.append(os.path.splitext(name)[0]) 

        val_data_size = 300

        #valid_ids = file_ids[:val_data_size]
        #train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d -----------" %(len(file_ids)))

        return file_ids

    def load_nigula_patch(self):
        #ROAD SIDE DATA
        self.path = "/home/raul/Desktop/L22neNigula/originals/"
        files = os.listdir(self.path)

        exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.'] 
        file_ids = []
        for name in files:
            if not any(exclude in name for exclude in exclude_list):
                file_ids.append(os.path.splitext(name)[0]) 

        val_data_size = 300

        #valid_ids = file_ids[:val_data_size]
        #train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d -----------" %(len(file_ids)))

        return file_ids

    
    def load_nigula_patch_2(self):
        #ROAD SIDE DATA
        self.path = "/home/raul/Desktop/L22neNigula/Manual/original/"
        files = os.listdir(self.path)

        exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.'] 
        file_ids = []
        for name in files:
            if not any(exclude in name for exclude in exclude_list):
                file_ids.append(os.path.splitext(name)[0]) 

        val_data_size = 300

        #valid_ids = file_ids[:val_data_size]
        #train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d -----------" %(len(file_ids)))

        return file_ids


    def load_turns_shaded_patch(self):
        #ROAD SIDE DATA
        self.path = "/home/raul/Desktop/L22neNigula/Manual/rdmask/"
        files = os.listdir(self.path)

        exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.'] 
        file_ids = []
        for name in files:
            if not any(exclude in name for exclude in exclude_list):
                file_ids.append(os.path.splitext(name)[0]) 

        val_data_size = 300

        print("----------- files for validation %d -----------" %(len(file_ids)))

        return file_ids


    def load_shtroads_patch(self):
        #ROAD SIDE DATA
        self.path = "/home/raul/Desktop/mahb/INPUT/shtroads/"
        files = os.listdir(self.path)

        exclude_list = ['.info.', '.vrt', '.mask.', '.rdmask.'] 
        file_ids = []
        for name in files:
            if not any(exclude in name for exclude in exclude_list):
                file_ids.append(os.path.splitext(name)[0]) 

        val_data_size = 300

        #valid_ids = file_ids[:val_data_size]
        #train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d -----------" %(len(file_ids)))

        return file_ids


    def load_filtered_rotated_data(self):
        self.path = "/media/raul/Väline HDD/TTY/rotated/"
        files = os.listdir(self.path + 'rdmask')

        file_ids = []
        for name in files:
            file_ids.append(os.path.splitext(name)[0]) 

        val_data_size = 1600

        valid_ids = file_ids[:val_data_size]
        train_ids = file_ids[val_data_size:]

        print("----------- files for validation %d, files for training %d -----------" %(len(valid_ids), len(train_ids)))

        return train_ids, valid_ids