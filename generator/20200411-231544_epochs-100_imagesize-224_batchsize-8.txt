/home/raul/mahb/alfa/files/TTY/20180815/
No shuffle
Training begin time: 2020.04.11 23:15:44
WARNING:tensorflow:From <ipython-input-8-cc86d7734f7e>:16: Model.fit_generator (from tensorflow.python.keras.engine.training) is deprecated and will be removed in a future version.
Instructions for updating:
Please use Model.fit, which supports generators.
WARNING:tensorflow:sample_weight modes were coerced from
  ...
    to  
  ['...']
WARNING:tensorflow:sample_weight modes were coerced from
  ...
    to  
  ['...']
Train for 999 steps, validate for 100 steps
Epoch 1/100
999/999 [==============================] - 1290s 1s/step - loss: 0.0959 - acc: 0.9554 - iou_loss: -0.5892 - val_loss: 0.1284 - val_acc: 0.9538 - val_iou_loss: -0.7129
Epoch 2/100
999/999 [==============================] - 553s 553ms/step - loss: 0.0799 - acc: 0.9645 - iou_loss: -0.6470 - val_loss: 0.0925 - val_acc: 0.9583 - val_iou_loss: -0.7350
Epoch 3/100
999/999 [==============================] - 551s 551ms/step - loss: 0.0539 - acc: 0.9781 - iou_loss: -0.7577 - val_loss: 0.0830 - val_acc: 0.9663 - val_iou_loss: -0.7469
Epoch 4/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0454 - acc: 0.9827 - iou_loss: -0.8040 - val_loss: 0.0717 - val_acc: 0.9710 - val_iou_loss: -0.7906
Epoch 5/100
999/999 [==============================] - 549s 549ms/step - loss: 0.0404 - acc: 0.9847 - iou_loss: -0.8294 - val_loss: 0.0806 - val_acc: 0.9667 - val_iou_loss: -0.7288
Epoch 6/100
999/999 [==============================] - 548s 548ms/step - loss: 0.0329 - acc: 0.9872 - iou_loss: -0.8517 - val_loss: 0.0708 - val_acc: 0.9758 - val_iou_loss: -0.8355
Epoch 7/100
999/999 [==============================] - 549s 550ms/step - loss: 0.0329 - acc: 0.9880 - iou_loss: -0.8603 - val_loss: 0.0763 - val_acc: 0.9748 - val_iou_loss: -0.8288
Epoch 8/100
999/999 [==============================] - 550s 550ms/step - loss: 0.0308 - acc: 0.9886 - iou_loss: -0.8669 - val_loss: 0.0655 - val_acc: 0.9775 - val_iou_loss: -0.8445
Epoch 9/100
999/999 [==============================] - 551s 551ms/step - loss: 0.0275 - acc: 0.9894 - iou_loss: -0.8796 - val_loss: 0.0706 - val_acc: 0.9803 - val_iou_loss: -0.8685
Epoch 10/100
999/999 [==============================] - 549s 550ms/step - loss: 0.0301 - acc: 0.9888 - iou_loss: -0.8762 - val_loss: 0.0606 - val_acc: 0.9791 - val_iou_loss: -0.8470
Epoch 11/100
999/999 [==============================] - 548s 549ms/step - loss: 0.0262 - acc: 0.9898 - iou_loss: -0.8848 - val_loss: 0.0616 - val_acc: 0.9796 - val_iou_loss: -0.8496
Epoch 12/100
999/999 [==============================] - 549s 550ms/step - loss: 0.0258 - acc: 0.9903 - iou_loss: -0.8877 - val_loss: 0.0600 - val_acc: 0.9776 - val_iou_loss: -0.8334
Epoch 13/100
999/999 [==============================] - 552s 552ms/step - loss: 0.0256 - acc: 0.9905 - iou_loss: -0.8902 - val_loss: 0.0608 - val_acc: 0.9788 - val_iou_loss: -0.8393
Epoch 14/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0237 - acc: 0.9912 - iou_loss: -0.8968 - val_loss: 0.0675 - val_acc: 0.9751 - val_iou_loss: -0.8130
Epoch 15/100
999/999 [==============================] - 547s 548ms/step - loss: 0.0246 - acc: 0.9910 - iou_loss: -0.8969 - val_loss: 0.0685 - val_acc: 0.9804 - val_iou_loss: -0.8687
Epoch 16/100
999/999 [==============================] - 550s 550ms/step - loss: 0.0225 - acc: 0.9916 - iou_loss: -0.9014 - val_loss: 0.0564 - val_acc: 0.9798 - val_iou_loss: -0.8564
Epoch 17/100
999/999 [==============================] - 552s 552ms/step - loss: 0.0234 - acc: 0.9913 - iou_loss: -0.8993 - val_loss: 0.0567 - val_acc: 0.9810 - val_iou_loss: -0.8690
Epoch 18/100
999/999 [==============================] - 551s 551ms/step - loss: 0.0209 - acc: 0.9925 - iou_loss: -0.9085 - val_loss: 0.0530 - val_acc: 0.9802 - val_iou_loss: -0.8497
Epoch 19/100
999/999 [==============================] - 551s 552ms/step - loss: 0.0214 - acc: 0.9919 - iou_loss: -0.9090 - val_loss: 0.0597 - val_acc: 0.9794 - val_iou_loss: -0.8603
Epoch 20/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0222 - acc: 0.9918 - iou_loss: -0.9066 - val_loss: 0.0588 - val_acc: 0.9810 - val_iou_loss: -0.8715
Epoch 21/100
999/999 [==============================] - 548s 549ms/step - loss: 0.0221 - acc: 0.9922 - iou_loss: -0.9081 - val_loss: 0.0744 - val_acc: 0.9777 - val_iou_loss: -0.8332
Epoch 22/100
999/999 [==============================] - 549s 550ms/step - loss: 0.0204 - acc: 0.9929 - iou_loss: -0.9149 - val_loss: 0.0591 - val_acc: 0.9805 - val_iou_loss: -0.8601
Epoch 23/100
999/999 [==============================] - 550s 550ms/step - loss: 0.0208 - acc: 0.9919 - iou_loss: -0.9073 - val_loss: 0.0620 - val_acc: 0.9818 - val_iou_loss: -0.8766
Epoch 24/100
999/999 [==============================] - 552s 552ms/step - loss: 0.0197 - acc: 0.9930 - iou_loss: -0.9164 - val_loss: 0.0608 - val_acc: 0.9817 - val_iou_loss: -0.8692
Epoch 25/100
999/999 [==============================] - 553s 554ms/step - loss: 0.0201 - acc: 0.9926 - iou_loss: -0.9156 - val_loss: 0.0560 - val_acc: 0.9808 - val_iou_loss: -0.8606
Epoch 26/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0196 - acc: 0.9927 - iou_loss: -0.9172 - val_loss: 0.0587 - val_acc: 0.9822 - val_iou_loss: -0.8722
Epoch 27/100
999/999 [==============================] - 551s 552ms/step - loss: 0.0197 - acc: 0.9928 - iou_loss: -0.9187 - val_loss: 0.0588 - val_acc: 0.9804 - val_iou_loss: -0.8543
Epoch 28/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0182 - acc: 0.9931 - iou_loss: -0.9196 - val_loss: 0.0632 - val_acc: 0.9771 - val_iou_loss: -0.7917
Epoch 29/100
999/999 [==============================] - 550s 550ms/step - loss: 0.0176 - acc: 0.9934 - iou_loss: -0.9212 - val_loss: 0.0823 - val_acc: 0.9686 - val_iou_loss: -0.7685
Epoch 30/100
999/999 [==============================] - 549s 549ms/step - loss: 0.0172 - acc: 0.9933 - iou_loss: -0.9244 - val_loss: 0.0596 - val_acc: 0.9816 - val_iou_loss: -0.8742
Epoch 31/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0177 - acc: 0.9935 - iou_loss: -0.9249 - val_loss: 0.0542 - val_acc: 0.9824 - val_iou_loss: -0.8757
Epoch 32/100
999/999 [==============================] - 551s 552ms/step - loss: 0.0172 - acc: 0.9936 - iou_loss: -0.9257 - val_loss: 0.0624 - val_acc: 0.9803 - val_iou_loss: -0.8716
Epoch 33/100
999/999 [==============================] - 551s 551ms/step - loss: 0.0183 - acc: 0.9934 - iou_loss: -0.9250 - val_loss: 0.0581 - val_acc: 0.9802 - val_iou_loss: -0.8528
Epoch 34/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0194 - acc: 0.9927 - iou_loss: -0.9202 - val_loss: 0.0621 - val_acc: 0.9771 - val_iou_loss: -0.8251
Epoch 35/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0168 - acc: 0.9934 - iou_loss: -0.9253 - val_loss: 0.0537 - val_acc: 0.9819 - val_iou_loss: -0.8664
Epoch 36/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0162 - acc: 0.9937 - iou_loss: -0.9272 - val_loss: 0.0597 - val_acc: 0.9799 - val_iou_loss: -0.8706
Epoch 37/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0171 - acc: 0.9937 - iou_loss: -0.9259 - val_loss: 0.0650 - val_acc: 0.9809 - val_iou_loss: -0.8734
Epoch 38/100
999/999 [==============================] - 552s 552ms/step - loss: 0.0162 - acc: 0.9938 - iou_loss: -0.9290 - val_loss: 0.0569 - val_acc: 0.9815 - val_iou_loss: -0.8721
Epoch 39/100
999/999 [==============================] - 551s 551ms/step - loss: 0.0174 - acc: 0.9938 - iou_loss: -0.9268 - val_loss: 0.0545 - val_acc: 0.9820 - val_iou_loss: -0.8721
Epoch 40/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0178 - acc: 0.9936 - iou_loss: -0.9276 - val_loss: 0.0571 - val_acc: 0.9812 - val_iou_loss: -0.8754
Epoch 41/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0167 - acc: 0.9936 - iou_loss: -0.9278 - val_loss: 0.0639 - val_acc: 0.9812 - val_iou_loss: -0.8747
Epoch 42/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0154 - acc: 0.9940 - iou_loss: -0.9287 - val_loss: 0.0599 - val_acc: 0.9813 - val_iou_loss: -0.8761
Epoch 43/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0168 - acc: 0.9937 - iou_loss: -0.9273 - val_loss: 0.0580 - val_acc: 0.9823 - val_iou_loss: -0.8789
Epoch 44/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0179 - acc: 0.9935 - iou_loss: -0.9243 - val_loss: 0.0556 - val_acc: 0.9810 - val_iou_loss: -0.8652
Epoch 45/100
999/999 [==============================] - 551s 552ms/step - loss: 0.0156 - acc: 0.9941 - iou_loss: -0.9317 - val_loss: 0.0660 - val_acc: 0.9770 - val_iou_loss: -0.8461
Epoch 46/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0144 - acc: 0.9942 - iou_loss: -0.9333 - val_loss: 0.0678 - val_acc: 0.9828 - val_iou_loss: -0.8845
Epoch 47/100
999/999 [==============================] - 549s 549ms/step - loss: 0.0155 - acc: 0.9941 - iou_loss: -0.9308 - val_loss: 0.0602 - val_acc: 0.9809 - val_iou_loss: -0.8727
Epoch 48/100
999/999 [==============================] - 550s 550ms/step - loss: 0.0162 - acc: 0.9939 - iou_loss: -0.9302 - val_loss: 0.0572 - val_acc: 0.9823 - val_iou_loss: -0.8774
Epoch 49/100
999/999 [==============================] - 548s 548ms/step - loss: 0.0148 - acc: 0.9944 - iou_loss: -0.9338 - val_loss: 0.0568 - val_acc: 0.9831 - val_iou_loss: -0.8743
Epoch 50/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0152 - acc: 0.9945 - iou_loss: -0.9351 - val_loss: 0.0626 - val_acc: 0.9821 - val_iou_loss: -0.8764
Epoch 51/100
999/999 [==============================] - 553s 553ms/step - loss: 0.0139 - acc: 0.9947 - iou_loss: -0.9369 - val_loss: 0.0550 - val_acc: 0.9817 - val_iou_loss: -0.8713
Epoch 52/100
999/999 [==============================] - 553s 554ms/step - loss: 0.0145 - acc: 0.9942 - iou_loss: -0.9350 - val_loss: 0.0616 - val_acc: 0.9820 - val_iou_loss: -0.8791
Epoch 53/100
999/999 [==============================] - 547s 548ms/step - loss: 0.0154 - acc: 0.9944 - iou_loss: -0.9348 - val_loss: 0.0641 - val_acc: 0.9824 - val_iou_loss: -0.8827
Epoch 54/100
999/999 [==============================] - 551s 551ms/step - loss: 0.0136 - acc: 0.9947 - iou_loss: -0.9397 - val_loss: 0.0644 - val_acc: 0.9814 - val_iou_loss: -0.8667
Epoch 55/100
999/999 [==============================] - 549s 549ms/step - loss: 0.0138 - acc: 0.9948 - iou_loss: -0.9382 - val_loss: 0.0651 - val_acc: 0.9812 - val_iou_loss: -0.8777
Epoch 56/100
999/999 [==============================] - 548s 548ms/step - loss: 0.0153 - acc: 0.9943 - iou_loss: -0.9348 - val_loss: 0.0659 - val_acc: 0.9824 - val_iou_loss: -0.8848
Epoch 57/100
999/999 [==============================] - 550s 550ms/step - loss: 0.0134 - acc: 0.9948 - iou_loss: -0.9381 - val_loss: 0.0681 - val_acc: 0.9823 - val_iou_loss: -0.8849
Epoch 58/100
999/999 [==============================] - 548s 548ms/step - loss: 0.0152 - acc: 0.9947 - iou_loss: -0.9366 - val_loss: 0.0586 - val_acc: 0.9826 - val_iou_loss: -0.8727
Epoch 59/100
999/999 [==============================] - 549s 549ms/step - loss: 0.0163 - acc: 0.9941 - iou_loss: -0.9334 - val_loss: 0.0624 - val_acc: 0.9821 - val_iou_loss: -0.8789
Epoch 60/100
999/999 [==============================] - 553s 553ms/step - loss: 0.0124 - acc: 0.9951 - iou_loss: -0.9427 - val_loss: 0.0602 - val_acc: 0.9824 - val_iou_loss: -0.8787
Epoch 61/100
999/999 [==============================] - 552s 552ms/step - loss: 0.0137 - acc: 0.9949 - iou_loss: -0.9392 - val_loss: 0.0666 - val_acc: 0.9833 - val_iou_loss: -0.8884
Epoch 62/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0141 - acc: 0.9948 - iou_loss: -0.9403 - val_loss: 0.0631 - val_acc: 0.9807 - val_iou_loss: -0.8708
Epoch 63/100
999/999 [==============================] - 551s 552ms/step - loss: 0.0122 - acc: 0.9954 - iou_loss: -0.9447 - val_loss: 0.0585 - val_acc: 0.9824 - val_iou_loss: -0.8733
Epoch 64/100
999/999 [==============================] - 551s 552ms/step - loss: 0.0144 - acc: 0.9947 - iou_loss: -0.9390 - val_loss: 0.0710 - val_acc: 0.9783 - val_iou_loss: -0.8555
Epoch 65/100
999/999 [==============================] - 553s 553ms/step - loss: 0.0134 - acc: 0.9948 - iou_loss: -0.9404 - val_loss: 0.0650 - val_acc: 0.9822 - val_iou_loss: -0.8749
Epoch 66/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0155 - acc: 0.9940 - iou_loss: -0.9328 - val_loss: 0.0602 - val_acc: 0.9806 - val_iou_loss: -0.8572
Epoch 67/100
999/999 [==============================] - 554s 554ms/step - loss: 0.0153 - acc: 0.9944 - iou_loss: -0.9364 - val_loss: 0.0558 - val_acc: 0.9805 - val_iou_loss: -0.8462
Epoch 68/100
999/999 [==============================] - 567s 568ms/step - loss: 0.0139 - acc: 0.9947 - iou_loss: -0.9384 - val_loss: 0.0590 - val_acc: 0.9809 - val_iou_loss: -0.8694
Epoch 69/100
999/999 [==============================] - 606s 606ms/step - loss: 0.0135 - acc: 0.9950 - iou_loss: -0.9407 - val_loss: 0.0701 - val_acc: 0.9818 - val_iou_loss: -0.8823
Epoch 70/100
999/999 [==============================] - 565s 565ms/step - loss: 0.0133 - acc: 0.9949 - iou_loss: -0.9411 - val_loss: 0.0674 - val_acc: 0.9832 - val_iou_loss: -0.8893
Epoch 71/100
999/999 [==============================] - 549s 550ms/step - loss: 0.0131 - acc: 0.9947 - iou_loss: -0.9389 - val_loss: 0.0886 - val_acc: 0.9788 - val_iou_loss: -0.8658
Epoch 72/100
999/999 [==============================] - 548s 549ms/step - loss: 0.0113 - acc: 0.9954 - iou_loss: -0.9455 - val_loss: 0.0630 - val_acc: 0.9805 - val_iou_loss: -0.8673
Epoch 73/100
999/999 [==============================] - 548s 549ms/step - loss: 0.0125 - acc: 0.9951 - iou_loss: -0.9416 - val_loss: 0.0654 - val_acc: 0.9791 - val_iou_loss: -0.8580
Epoch 74/100
999/999 [==============================] - 548s 549ms/step - loss: 0.0131 - acc: 0.9953 - iou_loss: -0.9438 - val_loss: 0.0798 - val_acc: 0.9801 - val_iou_loss: -0.8720
Epoch 75/100
999/999 [==============================] - 548s 548ms/step - loss: 0.0127 - acc: 0.9950 - iou_loss: -0.9427 - val_loss: 0.0579 - val_acc: 0.9794 - val_iou_loss: -0.8586
Epoch 76/100
999/999 [==============================] - 548s 548ms/step - loss: 0.0117 - acc: 0.9952 - iou_loss: -0.9443 - val_loss: 0.0562 - val_acc: 0.9829 - val_iou_loss: -0.8820
Epoch 77/100
999/999 [==============================] - 553s 553ms/step - loss: 0.0137 - acc: 0.9949 - iou_loss: -0.9407 - val_loss: 0.0640 - val_acc: 0.9807 - val_iou_loss: -0.8625
Epoch 78/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0135 - acc: 0.9948 - iou_loss: -0.9409 - val_loss: 0.0707 - val_acc: 0.9815 - val_iou_loss: -0.8843
Epoch 79/100
999/999 [==============================] - 551s 552ms/step - loss: 0.0124 - acc: 0.9953 - iou_loss: -0.9439 - val_loss: 0.0653 - val_acc: 0.9808 - val_iou_loss: -0.8740
Epoch 80/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0126 - acc: 0.9953 - iou_loss: -0.9442 - val_loss: 0.0753 - val_acc: 0.9793 - val_iou_loss: -0.8638
Epoch 81/100
999/999 [==============================] - 549s 549ms/step - loss: 0.0129 - acc: 0.9952 - iou_loss: -0.9428 - val_loss: 0.0648 - val_acc: 0.9814 - val_iou_loss: -0.8688
Epoch 82/100
999/999 [==============================] - 548s 549ms/step - loss: 0.0113 - acc: 0.9955 - iou_loss: -0.9464 - val_loss: 0.0813 - val_acc: 0.9754 - val_iou_loss: -0.8338
Epoch 83/100
999/999 [==============================] - 549s 550ms/step - loss: 0.0121 - acc: 0.9954 - iou_loss: -0.9457 - val_loss: 0.0708 - val_acc: 0.9834 - val_iou_loss: -0.8947
Epoch 84/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0119 - acc: 0.9952 - iou_loss: -0.9459 - val_loss: 0.0641 - val_acc: 0.9828 - val_iou_loss: -0.8831
Epoch 85/100
999/999 [==============================] - 550s 550ms/step - loss: 0.0119 - acc: 0.9952 - iou_loss: -0.9451 - val_loss: 0.0551 - val_acc: 0.9823 - val_iou_loss: -0.8760
Epoch 86/100
999/999 [==============================] - 550s 551ms/step - loss: 0.0123 - acc: 0.9954 - iou_loss: -0.9458 - val_loss: 0.0621 - val_acc: 0.9799 - val_iou_loss: -0.8608
Epoch 87/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0109 - acc: 0.9955 - iou_loss: -0.9477 - val_loss: 0.0628 - val_acc: 0.9827 - val_iou_loss: -0.8834
Epoch 88/100
999/999 [==============================] - 552s 552ms/step - loss: 0.0103 - acc: 0.9959 - iou_loss: -0.9498 - val_loss: 0.0743 - val_acc: 0.9833 - val_iou_loss: -0.8918
Epoch 89/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0106 - acc: 0.9955 - iou_loss: -0.9482 - val_loss: 0.0691 - val_acc: 0.9823 - val_iou_loss: -0.8853
Epoch 90/100
999/999 [==============================] - 553s 553ms/step - loss: 0.0109 - acc: 0.9955 - iou_loss: -0.9479 - val_loss: 0.0599 - val_acc: 0.9817 - val_iou_loss: -0.8766
Epoch 91/100
999/999 [==============================] - 553s 553ms/step - loss: 0.0107 - acc: 0.9956 - iou_loss: -0.9481 - val_loss: 0.0608 - val_acc: 0.9829 - val_iou_loss: -0.8869
Epoch 92/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0105 - acc: 0.9957 - iou_loss: -0.9497 - val_loss: 0.0626 - val_acc: 0.9822 - val_iou_loss: -0.8801
Epoch 93/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0118 - acc: 0.9952 - iou_loss: -0.9458 - val_loss: 0.0706 - val_acc: 0.9828 - val_iou_loss: -0.8850
Epoch 94/100
999/999 [==============================] - 552s 553ms/step - loss: 0.0099 - acc: 0.9959 - iou_loss: -0.9505 - val_loss: 0.0624 - val_acc: 0.9827 - val_iou_loss: -0.8807
Epoch 95/100
999/999 [==============================] - 552s 552ms/step - loss: 0.0097 - acc: 0.9959 - iou_loss: -0.9505 - val_loss: 0.0691 - val_acc: 0.9817 - val_iou_loss: -0.8791
Epoch 96/100
999/999 [==============================] - 553s 553ms/step - loss: 0.0114 - acc: 0.9955 - iou_loss: -0.9483 - val_loss: 0.0718 - val_acc: 0.9812 - val_iou_loss: -0.8795
Epoch 97/100
999/999 [==============================] - 551s 552ms/step - loss: 0.0108 - acc: 0.9957 - iou_loss: -0.9499 - val_loss: 0.0801 - val_acc: 0.9799 - val_iou_loss: -0.8640
Epoch 98/100
999/999 [==============================] - 548s 549ms/step - loss: 0.0118 - acc: 0.9956 - iou_loss: -0.9472 - val_loss: 0.0691 - val_acc: 0.9817 - val_iou_loss: -0.8754
Epoch 99/100
999/999 [==============================] - 550s 550ms/step - loss: 0.0108 - acc: 0.9957 - iou_loss: -0.9478 - val_loss: 0.0553 - val_acc: 0.9832 - val_iou_loss: -0.8868
Epoch 100/100
999/999 [==============================] - 551s 552ms/step - loss: 0.0109 - acc: 0.9956 - iou_loss: -0.9484 - val_loss: 0.0688 - val_acc: 0.9780 - val_iou_loss: -0.8527
Training end time: 2020.04.12 14:47:08