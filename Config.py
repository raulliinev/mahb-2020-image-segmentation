import logging
import random
import numpy as np
import tensorflow as tf

#logging.basicConfig(filename='segmentation.log', filemode='a', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt = '%m/%d/%Y %I:%M:%S %p')

seed = 2020
random.seed = seed
np.random.seed = seed
tf.seed = seed


def get_logger(    
        LOG_FORMAT     = '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        LOG_NAME       = 'MAHB',
        LOG_FILE_INFO  = 'info.log',
        LOG_FILE_ERROR = 'error.log'):

    log           = logging.getLogger(LOG_NAME)
    log_formatter = logging.Formatter(LOG_FORMAT)

    # comment this to suppress console output
    #stream_handler = logging.StreamHandler()
    #stream_handler.setFormatter(log_formatter)
    #log.addHandler(stream_handler)

    file_handler_info = logging.FileHandler(LOG_FILE_INFO, mode='a')
    file_handler_info.setFormatter(log_formatter)
    file_handler_info.setLevel(logging.INFO)
    log.addHandler(file_handler_info)

    file_handler_error = logging.FileHandler(LOG_FILE_ERROR, mode='a')
    file_handler_error.setFormatter(log_formatter)
    file_handler_error.setLevel(logging.ERROR)
    log.addHandler(file_handler_error)

    log.setLevel(logging.INFO)

    return log