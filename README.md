# Identifying road on orthoframes with U-Net image segmentation architecture

## Project
Specify test dataset and model in file
```bash
python scripts/predict.py
```
```bash
python scripts/iougen_multiple.py
```

## Jupyter
jupyter nbconvert --to script project/segmentation.ipynb 

## Scripts
### Change filenames
```bash
for f in *.jpg; do mv -- "$f" "MANUAL_${f%.jpg}.jpg"; done
print s
```

### Search Latex code for percentage nr
```bash
grep -r -E "[0-9]{2}\.[0-9]{2}"
```