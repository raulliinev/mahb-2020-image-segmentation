import os
import logging
import cv2

import numpy as np
from tensorflow import keras

class DataGen(keras.utils.Sequence):
    def __init__(self, ids, path, batch_size=8, image_size=224, custom_path = False):
        self.ids = ids
        self.path = path
        self.batch_size = batch_size
        self.image_size = image_size
        self.on_epoch_end()
        self.original_images = []
        self.masks = []
        self.rdmasks = []
        self.file_ids = []
        self.current_batch_images = ''
        self.custom_rdmask_path = custom_path

    def __load__(self, id_name):

        image_path = self.path + id_name + ".jpg"

        if os.path.exists(image_path) == False:
            logging.error(image_path + " does not exist")
            return None, None

        ## Reading Image
        image = cv2.imread(image_path, 1)

        try:
            image = cv2.resize(image, (self.image_size, self.image_size))
        except Exception as e:
            logging.info(image_path)
            logging.exception(e)
            return None, None
            
        ## Normalizing 
        image = image/255.0
        
        return image

    def __getitem__(self, index, getimagepath = False):
        if(index+1)*self.batch_size > len(self.ids):
            self.batch_size = len(self.ids) - index*self.batch_size
        
        files_batch = self.ids[index*self.batch_size : (index+1)*self.batch_size]

        if getimagepath is True:
            self.current_batch_images = files_batch
        
        image = []
        mask  = []

        count = 0
        
        for file_id in files_batch:
            _img = self.__load__(file_id)
            
            if _img is None:
                continue

            image.append(_img)
            
        image = np.array(image)
        
        return image
    
    def on_epoch_end(self):
        pass
    
    def __len__(self):
        return int(np.ceil(len(self.ids)/float(self.batch_size)))