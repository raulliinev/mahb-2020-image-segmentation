import os

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import backend as K
import numpy as np
import cv2
from Config import get_logger

logger = get_logger()

class DataGen(keras.utils.Sequence):
    def __init__(self, ids, path, batch_size=5, image_size=224, custom_path = False, rotated = False):
        self.ids = ids
        self.path = path
        self.batch_size = batch_size
        self.image_size = image_size
        self.original_images = []
        self.masks = []
        self.rdmasks = []
        self.file_ids = []
        self.current_batch_images = ''
        self.custom_rdmask_path = custom_path
        self.rotated = rotated
        #self.on_epoch_end()

    def __load__(self, id_name):

        if self.rotated == True:
            image_path = os.path.join(self.path, 'original', id_name) + ".png"
            mask_path = os.path.join(self.path, 'rdmask', id_name) + '.png'
        elif self.custom_rdmask_path == False:
            image_path = os.path.join(self.path, id_name[:id_name.index("-")], id_name) + ".jpg"
            mask_path = os.path.join(self.path, id_name[:id_name.index("-")], id_name) + ".rdmask" + '.png'
        else:
            image_path = self.path + id_name + ".jpg"
            mask_path = self.custom_rdmask_path + id_name + ".png"

        if os.path.exists(image_path) == False or os.path.exists(mask_path) == False:
            logger.error(image_path + " or " + mask_path + " does not exist")
            return None, None

        ## Reading Image
        if os.path.exists(image_path) == False:
            print("PATH DOES NOT EXIST: " + image_path)

        image = cv2.imread(image_path, 1)

        if self.rotated == False:
            try:
                image = cv2.resize(image, (self.image_size, self.image_size))
            except Exception as e:
                logger.info(image_path)
                logger.exception(e)
                return None, None
        
        mask = np.zeros((self.image_size, self.image_size, 1))
        
        _mask_path = mask_path
        _mask_image = cv2.imread(_mask_path, -1)
        try:
            _mask_image = cv2.resize(_mask_image, (self.image_size, self.image_size)) #128x128
            _mask_image = np.expand_dims(_mask_image, axis=-1)
            mask = np.maximum(mask, _mask_image)
        except Exception as e:
            print("EXCEPTION: " + str(e))
            logger.info(mask_path)
            logger.exception(e)
            return None, None
            
        ## Normalizing 
        image = image/255.0
        mask = mask/255.0
        
        return image, mask

    def __getitem__(self, index, getimagepath = False):
        
        if(index+1)*self.batch_size > len(self.ids):
            self.batch_size = len(self.ids) - index*self.batch_size
        
        files_batch = self.ids[index*self.batch_size : (index+1)*self.batch_size]

        #print("BATCH Print")
        #print(str(files_batch))

        if getimagepath is True:
            self.current_batch_images = files_batch
        
        image = []
        mask  = []

        count = 0
        
        for file_id in files_batch:
            _img, _mask = self.__load__(file_id)
            
            if _img is None:
                continue

            image.append(_img)
            mask.append(_mask)
            
        image = np.array(image)
        mask  = np.array(mask)
        
        return image, mask
    
    def on_epoch_end(self):
        #logger.info(str(self.ids))
        np.random.shuffle(self.ids)
        #logger.info("PASSED EPOCH END")

    def on_epoch_begin(self):
        pass
    
    def __len__(self):
        #self.on_epoch_end()
        #logger.info("CEIL: " + str(int(np.ceil(len(self.ids)/float(self.batch_size)))))
        #logger.info("FLOOR: " + str(int(np.floor(len(self.ids)/float(self.batch_size)))))
        return int(np.ceil(len(self.ids)/float(self.batch_size)))